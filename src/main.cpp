#include <iostream>
#include <vector>
#include <fstream>
using namespace std;
 
#define space() for(int ll = 0; ll < 100; ll++) cout<<endl
 
class Candidato{
    private:
    string info[58];
 
    public:
 
    Candidato(string s){
        char c;
        int lastPos = 1;
        int currIndex = 0;
        for(int i = 2; i < s.size(); i++){
            c = s[i];
            if(c == '\"'){
                info[currIndex++] = s.substr(lastPos, i - lastPos);
                i += 3;
                lastPos = i;
            }
        }
    }
 
    string Get(int i){
        return info[i];
    }
 
    void ShowInfo(){
        string attrName[58] = {"DT_GERACAO", "HH_GERACAO", "ANO_ELEICAO", "CD_TIPO_ELEICAO",
            "NM_TIPO_ELEICAO", "NR_TURNO", "CD_ELEICAO", "DS_ELEICAO", "DT_ELEICAO",
            "TP_ABRANGENCIA", "SG_UF", "SG_UE", "NM_UE", "CD_CARGO", "DS_CARGO", "SQ_CANDIDATO",
            "NR_CANDIDATO", "NM_CANDIDATO", "NM_URNA_CANDIDATO", "NM_SOCIAL_CANDIDATO", "NR_CPF_CANDIDATO",
            "NM_EMAIL", "CD_SITUACAO_CANDIDATURA", "DS_SITUACAO_CANDIDATURA", "CD_DETALHE_SITUACAO_CAND",
            "DS_DETALHE_SITUACAO_CAND", "TP_AGREMIACAO", "NR_PARTIDO", "SG_PARTIDO", "NM_PARTIDO",
            "SQ_COLIGACAO", "NM_COLIGACAO", "DS_COMPOSICAO_COLIGACAO", "CD_NACIONALIDADE", "DS_NACIONALIDADE",
            "SG_UF_NASCIMENTO", "CD_MUNICIPIO_NASCIMENTO", "NM_MUNICIPIO_NASCIMENTO", "DT_NASCIMENTO",
            "NR_IDADE_DATA_POSSE", "NR_TITULO_ELEITORAL_CANDIDATO", "CD_GENERO", "DS_GENERO",
            "CD_GRAU_INSTRUCAO", "DS_GRAU_INSTRUCAO", "CD_ESTADO_CIVIL", "DS_ESTADO_CIVIL", "CD_COR_RACA",
            "DS_COR_RACA", "CD_OCUPACAO", "DS_OCUPACAO", "NR_DESPESA_MAX_CAMPANHA", "CD_SIT_TOT_TURNO",
            "DS_SIT_TOT_TURNO", "ST_REELEICAO", "ST_DECLARAR_BENS", "NR_PROTOCOLO_CANDIDATURA", "NR_PROCESSO"
        };
        for(int i = 0; i < 58; i++){
            cout<<attrName[i]<<": "<<info[i]<<endl;
        }
    }
};
 
class Eleitor{
    private:
    string nome;
    string numCandidato[5];
    string nomeCandidato[5];
    public:
    Eleitor(string _nome){
        nome = _nome;
    }
 
    string GetNome(){return nome;}
    string Get(int i){
        if(numCandidato[i] == "0") return nomeCandidato[i];
        return "(" + numCandidato[i] + ") " + nomeCandidato[i];
    }
    void Set(int i, string num, string nome){
        numCandidato[i] = num;
        nomeCandidato[i] = nome;
    }
 
};
 
vector<Candidato> candidatos;
vector<Eleitor> eleitores;
 
void ReadFile(){
    candidatos.clear();
    string line;
    ifstream myfile ("../data/consulta_cand_2018_DF.csv");
 
    if (myfile.is_open()){
        getline (myfile,line);
        while ( getline (myfile,line) ){
            candidatos.push_back(Candidato(line));
        }
        myfile.close();
    }
    else cout<<"problem with consulta_cand_2018_DF.csv"<<endl;
 
    ifstream myfile2 ("../data/consulta_cand_2018_BR.csv");
    if (myfile2.is_open()){
        getline (myfile2,line);
        while ( getline (myfile2,line) ){
            candidatos.push_back(Candidato(line));
        }
        myfile2.close();
    }
    else cout<<"problem with consulta_cand_2018_BR.csv"<<endl;
}
 
int FindCandidato(string num, string codCargo){
    //numero do candidato atributo 16
    //codigo do cargo atributo 13
    for(int i = 0; i < candidatos.size(); i++){
        if(num == candidatos[i].Get(16) && codCargo == candidatos[i].Get(13)) return i;
    }
    return -1;
}
 
int main() {
    ReadFile();
 
    eleitores.clear();
 
    string cod[] = {"8", "6", "5", "3", "1"};
    string candidatura[] = {"Deputado Distrital", "Deputado Estadual", "Senador", "Governador", "Presidente"};
    int tamanhoNumero[] = {5, 4, 3, 2, 2};
 
    cout<<"Digite o numero de eleitores que vao votar nessa urna: ";
    int numEleitores;
    cin>>numEleitores;
 
 
    string nome, nomeCand, numCand;
    string input;
    for(int i = 0; i < numEleitores; i++){
        space();
        cout<<"Digite seu nome: ";
        cin>>nome;
 
        Eleitor e(nome);
 
        for(int j = 0; j < 5; j++){
            space();
            input = "";
            cout<<"Digite o numero do seu candidato a "<<candidatura[j]<<endl;
            cout<<"Obs: para votar em branco, digite BRANCO"<<endl;
            cin>>numCand;
 
            space();
 
            //CASO VOTO BRANCO
            if(numCand == "BRANCO" || numCand == "branco"){
 
                while(1){
                    cout<<"Voce votou em branco"<<endl;
                    cout<<"Digite 1 para confirmar"<<endl;
                    cout<<"Digite 2 para corrigir"<<endl;
                    cin>>input;
                    if(input == "1"){
                        e.Set(j, "0", "Branco");
                        break;
                    }
                    else if(input == "2"){
                        j -= 1;
                        break;
                    }
                    else {
                        cout<<"Opcao invalida!"<<endl<<endl;
                    }
                }
            }
            else{
 
                int index = FindCandidato(numCand, cod[j]);
                //CASO VOTO NULO
                if(index == -1){
 
                    while(1){
                        cout<<"Voce votou nulo"<<endl;
                        cout<<"Digite 1 para confirmar"<<endl;
                        cout<<"Digite 2 para corrigir"<<endl;
                        cin>>input;
                        if(input == "1"){
                            e.Set(j, "0", "Nulo");
                            break;
                        }
                        else if(input == "2"){
                            j -= 1;
                            break;
                        }
                        else {
                            cout<<"Opcao invalida!"<<endl<<endl;
                        }
                    }
                }
                //CASO VOTO VALIDO
                else{
                    cout<<"Voce votou em "<<candidatos[index].Get(17)<<endl;
                    while(1){
                        cout<<"Digite 0 para obter informacoes sobre esse candidato"<<endl;
                        cout<<"Digite 1 para confirmar"<<endl;
                        cout<<"Digite 2 para corrigir"<<endl;
                        cin>>input;
                        if(input == "0"){
                            cout<<endl<<endl<<"======================================"<<endl;
                            candidatos[index].ShowInfo();
                            cout<<"======================================"<<endl<<endl;
                        }
                        else if(input == "1"){
                            e.Set(j, numCand, candidatos[index].Get(17));
                            break;
                        }
                        else if(input == "2"){
                            j -= 1;
                            break;
                        }
                        else {
                            cout<<"Opcao invalida!"<<endl<<endl;
                        }
                    }
                }
            }
 
        }
 
        eleitores.push_back(e);
 
    }
 
    cout<<endl<<endl<<"===============RELATORIO=============="<<endl;
    for(int i = 0; i < eleitores.size(); i++){
        cout<<"Eleitor: "<<eleitores[i].GetNome()<<endl;
        for(int j = 0; j < 5; j++){
            cout<<"\t"<<candidatura[j]<<": "<<eleitores[i].Get(j)<<endl;
        }
        if(i != eleitores.size() - 1) cout<<"-------------------------------\n";
    }
    cout<<"======================================"<<endl<<endl;
 
 
    return 0;
}