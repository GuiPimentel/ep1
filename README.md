# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```
* Para registrar um voto válido:
 
```sh
Insira a palavra correspondente ao voto(o número de votação do candidato desejado), confirme.
```
* Para anular um voto:

```sh
Insira uma palavra correspondente a nenhum candidato e confirme. 
```
 * Para votar em branco:
 
```sh
Digite "branco" e confirme.
```
 
 
 
## Funcionalidades do projeto

Inserir código do candidato

Leitura código de candidato

Apagar caractere

Corrigir

Confirma

Branco

Nulo

## Bugs e problemas

## Referências
